require 'rails_helper'

describe RecipeService do
  subject(:finder) { described_class.new }

  describe '#find_all' do
    it 'returns all recipes' do
      expected_recipes = %w[
        4dT8tcb6ukGSIg2YyuGEOm 5jy9hcMeEgQ4maKGqIOYW6
        2E8bc3VcJmA8OgmQsageas 437eO3ORCME46i02SeCW46
      ]

      VCR.use_cassette('contentful_list') do
        expect(finder.find_all.map(&:id)).to eq(expected_recipes)
      end
    end
  end

  describe '#find' do
    it 'returns recipe with given id' do
      VCR.use_cassette('contentful_entry') do
        recipe = finder.find('5jy9hcMeEgQ4maKGqIOYW6').fields
        expect(recipe[:title])
          .to eq('Tofu Saag Paneer with Buttery Toasted Pita')
        expect(recipe[:calories]).to eq(900)
      end
    end
  end
end
