require 'rails_helper'

describe 'Recipes', type: :request do
  describe 'list' do
    it 'lists all recipes' do
      VCR.use_cassette('contentful_list_request') do
        get '/'

        expect(response.body).to include('Tofu Saag Paneer')
      end
    end

    context 'when remote failure' do
      it 'returns 500' do
        allow_any_instance_of(RecipeService)
          .to receive(:find_all).and_raise(HTTP::ConnectionError)
        get '/'

        expect(response).to have_http_status(:internal_server_error)
        expect(response.body).to include('Failed to fetch data')
      end
    end
  end

  describe 'details' do
    it 'lists all recipes' do
      VCR.use_cassette('contentful_details_request') do
        get '/4dT8tcb6ukGSIg2YyuGEOm'

        expect(response.body).to include('Grilled Cheese 101') # title
        expect(response.body).to include('vegan') # tags
        expect(response.body).to include('788') # calories
      end
    end
  end
end
