class RecipesController < ApplicationController
  def index
    @recipes = finder.find_all
  end

  def show
    @recipe = finder.find(params[:id]).fields
    @image = @recipe[:photo].url(width: 1020, height: 500,
                                 format: 'jpg', quality: 100)
    @description = Kramdown::Document.new(@recipe[:description]).to_html
  end

  private

  def finder
    @finder ||= RecipeService.new
  end
end
