class ApplicationController < ActionController::Base
  rescue_from Contentful::Error, HTTP::ConnectionError, with: :remote_error

  def remote_error
    render file: 'public/500.html',
           status: :internal_server_error, layout: false
  end
end
