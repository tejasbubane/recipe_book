# This class makes call to Contentful API and returns recipe data
class RecipeService
  def find_all
    client.entries(content_type: 'recipe')
  end

  def find(id)
    client.entry(id)
  end

  private

  def client
    @client ||= Contentful::Client.new(credentials)
  end

  def credentials
    Rails.application.credentials.contentful
  end
end
