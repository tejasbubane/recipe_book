# README

[![pipeline status](https://gitlab.com/tejasbubane/recipe_book/badges/master/pipeline.svg)](https://gitlab.com/tejasbubane/recipe_book/-/commits/master)


### Project Requirements:

* Ruby version - `2.7.0`

* Sqlite database - https://www.sqlite.org/download.html


### Setup:

* Install all dependencies - `bundle install`

* Add encryption key (to be shared via email) to `config/master.key`

* Run the server - `bundle exec rails s`

* Run test suite - `bundle exec rspec`

* Run lint - `bundle exec rubocop`


### Decisions taken:

* Save `space_id` and `access_token` in Rails credentials.

* Description for recipes is markdown - so parse it to html.

* Not saving remote copy of data (no caching).

* No Webpacker - Since I am using bootstrap for minimal styling, webpacker seemed like overkill.

* Not handling pagination over contentful API.

PS: Most decisions are taken to keep project minimal.
